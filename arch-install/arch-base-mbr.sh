#!/bin/bash

# First you need to partition your disk THEN run this scritpt

#	Language and locale

ln -sf /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime
hwclock --systohc
sed -i '177s/.//' /etc/locale.gen
locale-gen

#	Some file stuff
echo "LANG=en_US.UTF-8" >> /etc/locale.conf

# Setting hostname

echo "GentooPC" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1	localhost" >> /etc/hosts
# echo "127.0.1.1 GentooPC.localdomain GentooPC" >> /etc/hosts
echo root:arch | chpasswd

pacman -S --noconfirm grub networkmanager network-manager-applet dialog mtools dosfstools reflector base-devel linux linux-headers linux-firmware alsa-utils openssh os-prober wget xorg-server xorg-xinit alacritty xterm 

mkinitcpio -p linux
# mkinitcpio -p linux-lts # Uncomment if you have install linux-lts kernel 
grub-install --target=i386-pc /dev/sda
# grub-install --target=i386-pc /dev/sdb # Uncomment this and comment the line before out if you disk is /dev/sdb if its anything else you will need to change!
grub-mkconfig -o /boot/grub/grub.cfg

#	Sytemd stuff

systemctl enable NetworkManager
systemctl enable sshd

#	The user
useradd -m -g users -G wheel,audio arch
echo arch:arch | chpasswd

echo "arch ALL=(ALL) ALL" >> /etc/sudoers.d/arch
# reboot
printf "You are done! Make sure to change the passwords for user and root! Now run 'umount -a' and then reboot.\n"

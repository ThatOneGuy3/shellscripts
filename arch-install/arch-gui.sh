#!/bin/bash

# Installing picom and nitrogen
sudo pacman --noconfirm -S picom pcmanfm base-devel neovim nitrogen qutebrowser # firefox

#	Dwm install

wget https://dl.suckless.org/dwm/dwm-6.2.tar.gz # Getting dwm source code
tar -xzvf dwm-6.2.tar.gz # Extracting source code
cd dwm-6.2
sudo make clean install # Installing it
cd $HOME # change back to home (for expample '/home/YOUR_USER')

#	Dmenu install

wget https://dl.suckless.org/tools/dmenu-5.0.tar.gz # Getting dmenu source code
tar -xzvf dmenu-5.0.tar.gz # Extracting source code
cd dmenu-5.0
sudo make clean install # Installing dmenu
cd $HOME

#	St install
wget https://dl.suckless.org/st/st-0.8.4.tar.gz
tar -xzvf st-0.8.4.tar.gz
cd st-0.8.4
sudo make clean install
cd $HOME

#	Installing aur helper paru
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si 
cd $HOME

#	Change .xinitrc file to start Dwm

echo "nitrogen --restore &" >> .xinitrc
echo "picom --no-vsync &" >> .xinitrc
echo "exec dwm" >> .xinitrc

#	Setup is done!

printf "\nSetup is Done! Run 'startx' to start dwm and enjoy!\n"

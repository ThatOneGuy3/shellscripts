#!/bin/bash

# This is the minimal install for the gui, you will need to install a brower, file manager etc.

sudo pacman -S --noconfirm neovim

#	Dwm install

wget https://dl.suckless.org/dwm/dwm-6.2.tar.gz
tar -xzvf dwm-6.2.tar.gz
cd dwm-6.2
sudo make clean install
cd $HOME

#	Dmenu install

wget https://dl.suckless.org/tools/dmenu-5.0.tar.gz
tar -xzvf dmenu-5.0.tar.gz
cd dmenu-5.0
sudo make clean install
cd $HOME

#	Installing St
wget https://dl.suckless.org/st/st-0.8.4.tar.gz
tar -xzvf st-0.8.4.tar.gz
cd st-0.8.4
sudo make clean install
cd $HOME

#	Change .xinitrc file to start Dwm

echo "exec dwm" > .xinitrc

printf "\nSetup is Done! Run 'startx' to start dwm and enjoy!\n"
